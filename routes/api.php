<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('auth/register', 'Auth\AuthController@register');
Route::post('auth/login', 'Auth\AuthController@login');
Route::group([

    'middleware' => 'auth:api',
    'prefix' => 'auth'

], function ($router) {


    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('user', 'Auth\\AuthController@me');
    Route::patch('user', 'API\\UserController@patch');
    //Diary
    Route::get('diary/date/{date}/user/{user_id?}','API\MealController@get');
    Route::delete('meal/{id}/delete','API\MealController@delete');
    Route::post('diary/meal/add','API\MealController@store');
    Route::patch('meal/{id}/update','API\MealController@update');
    Route::get('meal/{id}/','API\MealController@find');
    Route::get('diary/date/{date}/nutrition','API\\MealController@getNutrition');
    //Food
    Route::get('foods/food/{id}','Admin\FoodCrudController@getFood');
    Route::post('foods/food/add','Admin\FoodCrudController@storeFood');
    Route::get('foods/search/{keyword}','Admin\FoodCrudController@searchFoods');

    //Progress
    Route::get('progress/get/user/{user_id?}','API\ProgressController@get');
    Route::post('progress/add','API\ProgressController@store');
    Route::patch('progress/{id}/update','API\ProgressController@patch');
    Route::delete('progress/{id}/delete','API\ProgressController@delete');

    //Posts
    Route::get('posts','Admin\PostCrudController@getPosts');
    Route::get('posts/post/{id}','Admin\PostCrudController@getPost');
    //Friends
    Route::get('friends','API\FriendController@get');
    Route::get('friends/requests','API\FriendController@getRequests');
    Route::post('friends/add','API\FriendController@store');
    Route::patch('friends/requests/request/{id}/accept','API\FriendController@accept');
    Route::delete('friends/friend/requests/request{id}/decline','API\FriendController@delete');
    //User
    Route::get('users/search/{name}','API\UserController@get');
    Route::get('users/user/{id}','API\UserController@find');
    //Exercises
    Route::get('exercises/exercise/find/{name}','API\\ExerciseController@find');
    Route::get('exercises/exercise/add/','API\\ExerciseController@store');
    //Workouts
    Route::get('workouts/get','API\\WorkoutController@get');
    Route::post('workouts/workout/create','API\\WorkoutController@store');
    Route::get('workouts/workout/{workout_id}/get','API\\WorkoutController@find');


});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
