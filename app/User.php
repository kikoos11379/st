<?php

namespace App;

use App\Models\Friend;
use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Backpack\CRUD\app\Models\Traits\CrudTrait; // <------------------------------- this one
use Spatie\Permission\Traits\HasRoles;// <------

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use CrudTrait; // <----- this
    use HasRoles; //

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    protected $fillable = [
        'name', 'email', 'password', 'age', 'weight', 'height', 'gender', 'activity', 'goal', 'goal_weight'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function calories()
    {
        return $this->hasOne(Calorie::class);
    }

    public function progress()
    {
        return $this->hasMany(Progress::class);
    }

    public function updateCalories()
    {
        $m = $this->weight;
        $h=$this->height;
        $a= $this->age;
        $s =5;
        if($this->gender == 'f'){
            $s =-151;
        }
        $activity = $this->activity;

        $calories = round((((10*$m + 6.25*$h - 0.5*$a) + $s) * $activity)+$this->goal);
        $protein = round((0.3*$calories)/4);
        $fat = round((0.2*$calories)/9);
        $carbs = round((0.5*$calories)/4);
        return $this->calories()->update([
            'calories'=>$calories,
            'protein'=>$protein,
            'fat'=>$fat,
            'carbs'=>$carbs
        ]);
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function CreateMacros(User $user)
    {
        $m = $user->weight;
        $h = $user->height;
        $a = $user->age;
        $s = 5;
        if ($user->gender == 'f') {
            $s = -151;
        }
        $activity = $user->activity;

        $calories = round((((10 * $m + 6.25 * $h - 0.5 * $a) + $s) * $activity) + $user->goal);
        $protein = round((0.3 * $calories) / 4);
        $fat = round((0.2 * $calories) / 9);
        $carbs = round((0.5 * $calories) / 4);

        return $this->calories()->create([
            'user_id' => $user->id,
            'calories' => $calories,
            'protein' => $protein,
            'fat' => $fat,
            'carbs' => $carbs
        ]);
    }

    public function createProgress()
    {
        return $this->progress()->create([
            'user_id' => $this->id,
            'kg' => $this->weight,
            'date' => Carbon::createFromFormat("YYYY-MM-DD", $this->created_at)
        ]);
    }

    public function friends()
    {
        return $this->hasMany(Friend::class)->where('is_accepted', 1);
    }

    public function isFriend($user_id)
    {
        return Friend::where('user_id', auth('api')->user()->id)->where('friend_id', $user_id) ->where('is_friends', 1)
                ->orWhere('user_id', $user_id)->where('friend_id', auth('api')->user()->id)
                ->where('is_friends', 1)
                ->count() > 0;
    }


}
