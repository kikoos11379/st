<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Workout extends Model
{
    //
    protected $fillable = ['user_id','name','date'];

    public function exercises(){
      return $this->belongsToMany(Exercise::class,'workout_exercises');
    }

}
