<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Friend extends Model
{
    //
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
    public function friend(){
        return $this->belongsTo(User::class,'friend_id');
    }
    public static function getRequests(){
        return Friend::where('friend_id',auth('api')->user()->id)
            ->where('is_friends',0)
            ->latest()
            ->count();
    }

}
