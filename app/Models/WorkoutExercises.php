<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkoutExercises extends Model
{
    //
    protected $with = 'exercise';
    protected $table = 'workout_exercises';
    public function addExercise($workout_id,$exercise_id,$weight,$reps){
           return $this->create([
                'workout_id'=>$workout_id,
                'exercise_id'=>$exercise_id,
                'weight'=>$weight,
                'reps'=>$reps
            ]);
    }
    public function exercise(){
        return $this->hasOne(Exercise::class);
    }
}
