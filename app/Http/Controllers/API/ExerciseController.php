<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExerciseRequest;
use App\Models\Exercise;
use Illuminate\Http\Request;

class ExerciseController extends Controller
{
    //
    public function find($name){
        $exercises = Exercise::where('name','LIKE','%'.$name.'%')->latest()->get();
        return response()->json([
            'exercises'=>$exercises
        ]);
    }
    public function store(ExerciseRequest $request){
        $exercise = Exercise::create([$request->validated()]);
        return response()->json([
           'status'=>'OK'
        ],201);
    }
}
