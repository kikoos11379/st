<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\FriendRequest;
use App\Models\Friend;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FriendController extends Controller
{
    //

    public function get(){
        $friends = Friend::where('user_id',auth('api')->user()->id)->where('is_friends',1)
            ->orWhere('friend_id',auth('api')->user()->id)
            ->where('is_friends',1)
            ->latest()
            ->with('user','friend')
            ->get();
        foreach ($friends as $friend){
            if($friend->user->id != auth('api')->user()->id) {

                $fr = $friend->user;
                $friend["user"] = $friend->friend;
                $friend["friend"] = $fr;
            }
        }


        $requests = Friend::getRequests();
        return response()->json([
           'friends'=>$friends,
            'requests'=>$requests
        ]);
    }
    public function getRequests(){
        $requests = Friend::where('friend_id',auth('api')->user()->id)
            ->where('is_friends',0)
            ->latest()
            ->with('user','friend')
            ->get();
        return response()->json([
            'requests'=>$requests
        ]);
    }
    public function store(FriendRequest $request){
        $friend = new Friend();
        $friend->user_id = auth('api')->user()->id;
        $friend->friend_id = $request->friend_id;
        $friend->is_friends = 0;
        $friend->save();
    }
    public function accept($id){
        $friend = Friend::find($id);
        $friend->is_friends = 1;
        $friend->save();
    }
    public function decline($id){
        $friend = Friend::find($id);
        $friend->delete();
    }
}
