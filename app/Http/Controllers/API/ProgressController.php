<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProgressRequest;
use App\Progress;
use Illuminate\Http\Request;

class ProgressController extends Controller
{
    //
    public function get($user_id=null){
        $progress = null;
        if($user_id != null){
            $progress = Progress::where('user_id',$user_id)->latest()->paginate(10);
        }else{
            $progress = Progress::where('user_id',auth('api')->user()->id)->latest()->paginate(10);
        }
        return response()->json([
            'progress'=>$progress
        ]);
    }
    public function store(ProgressRequest $request){
        $progress = new Progress();
        $progress->user_id = auth('api')->user()->id;
        $progress->kg = $request->kg;
        $progress->date = $request->date;
        $progress->save();
        return response()->json([
            'status'=>'Ok',
            'id'=>$progress->id
        ]);
    }
    public function patch($id,ProgressRequest $request){
        $progress = Progress::find($id);
        if($progress->user_id != auth('api')->user()->id){
            return response()->json([
                'status'=>'Unauthorized action'
            ],403);
        }
        $progress->kg = $request->kg;
        $progress->date = $request->date;
        $progress->save();
        return response()->json([
            'status'=>'Ok'
        ]);

    }
    public function delete($id){
        Progress::find($id)->delete();
        return response()->json([
            'status'=>'Ok'
        ]);
    }
}
