<?php

namespace App\Http\Controllers\API;

use App\Calorie;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CalorieController extends Controller
{
    //
    public function get(){
        $calories = Calorie::where('user_id',auth('api')->user()->id)->first()->get();
        return response()->json([
           'calories'=>$calories
        ]);
    }
}
