<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\NotificationRequest;
use App\Models\Notification;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    //
    public function get(){
        $notifications = Notification::where('user_id',auth('api')->user()->id)
            ->where('read',0)
            ->latest()
            ->get();
        return response()->json([
           'notifications'=>$notifications
        ]);
    }
    public function store(NotificationRequest $request){
        $notification = New Notification();
        $notification->create([$request->validated()]);
        return response()->json([
            'status'=>'Ok'
        ]);
    }
    public function patch(NotificationRequest $request,$id){
        $notification = Notification::find($id);
        $notification->update($request->validated());
        return response()->json([
            'status'=>'Ok'
        ]);
    }
    public function delete($id){
        $notification = Notification::find($id);
        if($notification->user_id != auth('api')->user()->id){
            return response()->json([
                'status'=>'Unauthorized'
            ],403);
        }
        $notification->delete();

        return response()->json([
            'status'=>'Ok'
        ]);
    }
}
