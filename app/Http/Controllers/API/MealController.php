<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\MealRequest;
use App\Meal;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MealController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function find($id){
        $meal = Meal::where('user_id',auth('api')->user()->id)->where('id',$id)->with('food')->get();
        return response()->json([
           'meal'=>$meal
        ]);
    }
    public function get($date,$user_id=null){
        $meals = null;
        if($user_id!=null){
            $meals = Meal::where('date',Carbon::parse($date))->where('user_id',$user_id)->with('food')->get();
        }else{
            $meals = Meal::where('date',Carbon::parse($date))->where('user_id',auth('api')->user()->id)->with('food')->get();
        }
        $breakfast = [];
        $lunch = [];
        $dinner = [];
        $sumbr = 0;
        $sumlu = 0;
        $sumdi = 0;

        foreach ($meals as $meal){

            if($meal->meal_type == 1){
                array_push($breakfast,$meal);
                $sumbr+=($meal->food->calories/100)*$meal->amount;
            }else if($meal->meal_type == 2){
                array_push($lunch,$meal);
                $sumlu+=($meal->food->calories/100)*$meal->amount;
            }else{
                array_push($dinner,$meal);
                $sumdi+=($meal->food->calories/100)*$meal->amount;
            }
        }
        return response()->json([
            'breakfast'=>$breakfast,
            'lunch'=>$lunch,
            'dinner'=>$dinner,
            'sumbr'=>$sumbr,
            'sumlu'=>$sumlu,
            'sumdi'=>$sumdi,
        ]);
    }
    public function store(MealRequest $request){
        $meal = new Meal();
        $meal->food_id = $request->food_id;
        $meal->amount = $request->amount;
        $meal->user_id = auth('api')->user()->id;
        $meal->meal_type = $request->meal_type;
        $meal->date = $request->date;
        $meal->save();
        return response()->json([
           "status"=>"OK"
        ]);
    }
    public function update($id,Request $request){
        $meal = Meal::find($id);
        $meal->amount = $request->amount;
        $meal->save();
        return response()->json([
           "status"=>"OK"
        ]);
    }
    public function delete($id){
        Meal::find($id)->delete();
        return response()->json([
           "status"=>"OK"
        ]);
    }
    public function getNutrition($date){
        $meals = Meal::where('date',Carbon::parse($date))->where('user_id',auth('api')->user()->id)->with('food')->get();
        $protein = 0;
        $fat = 0;
        $carbs = 0;
        $calories = 0;
        foreach ($meals as $meal){
            $protein += ($meal->food->protein / 100)*$meal->amount;
            $fat += ($meal->food->fat / 100)*$meal->amount;
            $carbs += ($meal->food->carbs / 100)*$meal->amount;
            $calories += ($meal->food->calories / 100)*$meal->amount;
        }

        return response()->json([
           'protein'=>$protein,
           'carbs'=>$carbs,
           'fat'=>$fat,
           'calories'=>$calories,
            'macros'=>auth('api')->user()->calories
        ]);


    }
}
