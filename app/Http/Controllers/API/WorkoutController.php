<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\addExerciseRequest;
use App\Http\Requests\WorkoutRequest;
use App\Models\Workout;
use Illuminate\Http\Request;

class WorkoutController extends Controller
{
    //
    public function find($workout_id)
    {
        $workout = Workout::find($workout_id)->with('exercises')->get();


        $workout[0]->exercises->groupBy('exercises.exercise.id');



        return response()->json([
            'workout' => $workout[0]
        ]);
    }

    public function get()
    {
        $workouts = Workout::latest()->get();
        return response()->json([
            'workouts' => $workouts
        ]);
    }

    public function store(WorkoutRequest $request)
    {
        //$request['user_id'] = auth('api')->user()->id;
        $workout = new Workout();
        $workout->name = $request->name;
        $workout->date = $request->date;
        $workout->user_id = auth('api')->user()->id;
        $workout->save();
        return response()->json([
            'status' => 'OK'
        ], 200);
    }

    public function patch($workout_id, WorkoutRequest $request)
    {
        $workout = Workout::find($workout_id);
        if ($workout->user_id != auth('api')->user()->id) {
            return response('', 403);
        }
        $workout->update($request->except(['user_id']));
        return response()->json([
            'status' => 'OK'
        ], 200);
    }

    public function delete($workout_id)
    {
        $workout = Workout::find($workout_id);
        if ($workout->user_id != auth('api')->user()->id) {
            return response('', 403);
        }
        $workout->delete();
        return response()->json([
            'status' => 'OK'
        ], 200);
    }

    public function addExercise($workout_id, addExerciseRequest $request)
    {
        $workout = Workout::find($workout_id);
        if ($workout->user_id != auth('api')->user()->id) {
            return response('', 403);
        }
        $workout->exercises()->attach($request->exercise_id, $request->weight, $request->reps);
        return response()->json([
            'status' => 'OK'
        ]);
    }

    public function removeExercise($workout_id, $exercise_id)
    {
        $workout = Workout::find($workout_id);
        if ($workout->user_id != auth('api')->user()->id) {
            return response('', 403);
        }
        $workout->exercises()->detach($exercise_id);
        return response()->json([
            'status' => 'OK'
        ]);
    }

}
