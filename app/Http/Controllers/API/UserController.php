<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    //
    public function get($name){
        $users=User::where('name','LIKE','%'.$name.'%')->get();
        return response()->json([
           'users'=>$users
        ]);
    }
    public function find($id){
        $user = User::where('id',$id)->get();
        $user = $user->map(function(User $myuser){

            $myuser['is_friends'] = $myuser->isFriend($myuser->id);

            return $myuser;
        });

        return response()->json([
            'user'=>$user
        ]);
    }

    public function patch(UserRequest $request){
        $user = User::find(auth('api')->user()->id);
        $user->update($request->validated());
        $user->updateCalories();


        return response()->json([
            'status'=>'Ok'
        ],200);

    }

}
