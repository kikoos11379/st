<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\FoodRequest;
use App\Models\Food;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;


/**
 * Class FoodCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class FoodCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Food');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/food');
        $this->crud->setEntityNameStrings('food', 'food');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        //$this->crud->setFromDb();
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(FoodRequest::class);

        // TODO: remove setFromDb() and manually define Fields
       // $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
    public function getFood($id){
        $food = Food::find($id);
        return response()->json([
            "food"=>$food
        ]);
    }

    public function searchFoods($keyword){
        $foods = Food::where('name','LIKE','%'.$keyword.'%')->get();
        return response()->json([
            "foods"=>$foods
        ]);
    }
    public function storeFood(FoodRequest $request){
        $food = new Food();
        $food->create($request->only(['name','protein','carbs','fat','calories','brand']));
        return response()->json([
           "status"=>"OK"
        ]);
    }
}
