<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PostRequest;
use App\Models\Post;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PostCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PostCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Post');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/post');
        $this->crud->setEntityNameStrings('post', 'posts');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
       // $this->crud->setFromDb();
        $this->crud->addColumns([
            [
                'type'=>'image',
                'name'=>'image',
                'label'=>'Image'
            ],
            [
                'type'=>'text',
                'name'=>'title',
                'label'=>'Title'
            ],
            [
                'name'         => 'category', // name of relationship method in the model
                'type'         => 'relationship',
                'label'        => 'Category', // Table column heading
                 'entity'    => 'category', // the method that defines the relationship in your Model
                 'attribute' => 'title', // foreign key attribute that is shown to user
                 'model'     => App\Models\Category::class, // foreign key model
            ],

        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(PostRequest::class);

        // TODO: remove setFromDb() and manually define Fields
//        $this->crud->setFromDb();
        $this->crud->addFields([

            [
                'name'=>'category_id',
                'type'=>'relationship'
            ], [
                'name'=>'image',
                'type'=>'image'
            ],
            [
                'name'=>'title',
                'type'=>'text'
            ],
            [
                'name'=>'content',
                'type'=>'tinymce'
            ],
            [
                'name'=>'user_id',
                'type'=>'hidden',
                'value'=>backpack_user()->id
            ],

        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
    public function getPosts(){
        $posts = Post::latest()->with('category','user')->paginate(10);
        return response()->json([
           'posts'=>$posts
        ]);
    }
    public function getPost($id){
        $post = Post::find($id)->with('category','user')->get();
        return response()->json([
           'post'=>$post
        ]);
    }
}
