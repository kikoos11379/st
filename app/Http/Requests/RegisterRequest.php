<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|string',
            'password'=>'required|min:8',
            'email'=>'required|unique:users|email',
            'age'=>'required',
            'weight'=>'required',
            'height'=>'required',
            'gender'=>'required',
            'activity'=>'required',
            'goal'=>'required',
            'goal_weight'=>'required'
        ];
    }
}
