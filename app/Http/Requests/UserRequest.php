<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'email'=>'required',
            'age'=>'required',
            'weight'=>'required',
            'height'=>'required',
            'gender'=>'required',
            'activity'=>'required',
            'goal_weight'=>'required',
            'goal'=>'required',

        ];
    }
}
