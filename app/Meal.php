<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meal extends Model
{
    //
    protected $fillable = ['food_id','user_id','meal_type','amount'];

    public function food(){
        return  $this->belongsTo(Models\Food::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
}
