<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Progress extends Model
{
    //
    protected $fillable = ['kg','date','user_id'];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
