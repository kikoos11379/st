<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Calorie extends Model
{
    //
   protected $fillable = ['user_id','calories','protein','fat','carbs'];
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function CreateMacros(User $user){
        $m = $user->weight;
        $h=$user->height;
        $a= $user->age;
        $s =5;
        if($user->gender == 'f'){
            $s =-151;
        }
        $activity = $user->activity;

       $calories = round((((10*$m + 6.25*$h - 0.5*$a) + $s) * $activity)+$user->goal);
       $protein = round((0.3*$calories)/4);
       $fat = round((0.2*$calories)/9);
       $carbs = round((0.5*$calories)/4);

       return $this->create([
           'user_id'=>$user->id,
           'calories'=>$calories,
           'protein'=>$protein,
           'fat'=>$fat,
           'carbs'=>$carbs
       ]);

    }
    public function UpdateMacros(User $user){



    }

}
