import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
 const store = new Vuex.Store({
    state: {
        auth:false,
        meal_type:1,
        date:null
    },
    mutations: {
        setAuth (state) {
            state.auth = true;
        },
        removeAuth(state){
            state.auth = false;
        },
        setMealType(state,type){
            state.meal_type = type
        },
        setDate(state,date){
            state.date = date;
        }

    },
    getters:{
        getAuth:state => {
            return state.auth;
        },
        getMealType:state => {
            return state.meal_type;
        },
        getDate:state=>{
            return state.date;
        }
    },

});
export default store;
