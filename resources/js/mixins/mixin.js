import axios from 'axios'
import qs from 'qs'
let moment = require('moment');
moment.locale('bg');

    let mix = {
        methods: {
            ajaxCall(method, url, params = '') {
                this.start_loading();
                if (!url || !method) {
                    console.log('Please read the instruction')
                    return 'Please read the instruction'
                }
                let config = {"method": method,
                    "url": url,
                    "maxRedirects": 2,
                }
                config['headers'] = {//'Content-Type':'application/x-www-form-urlencoded',
                 'X-Requested-With': 'XMLHttpRequest',
                'Authorization':'Bearer '+localStorage.access_token}
                if ((method == 'POST')) {
                    if (!params) {
                        console.log('POST without body')
                        return 'POST without body'
                    } else {
                        config["data"] = qs.stringify(params);
                        //config["headers"] = {'content-type': 'application/x-www-form-urlencoded'};

                    }
                }
                    if ((method == 'PATCH')) {
                        if (!params) {
                            console.log('POST without body')
                            return 'POST without body'
                        } else {
                            config["data"] = qs.stringify(params);
                            //config["headers"] = {'content-type': 'application/x-www-form-urlencoded'};

                        }

                    }
                    if ((method == 'GET')) {
                        if (params) {
                            config["params"] = params
                        }
                    }
                    if ((method == 'DELETE')) {
                        if (params) {
                            config["params"] = params
                        }
                    }

                return axios(config)
                    .then((response) => {
                        try {
                            if (response.data != ''){
                                JSON.parse(response.data)
                            }
                        } catch (error) {
                            if (typeof  response.data == "string") {
                                console.error('Result is not JSON. '+error.message+' response: '+response.data)
                            }
                        }
                        return new Promise((resolve, reject) => {
                            resolve(response.data)
                        })
                    })
                    .catch((err) => {
                        return new Promise((resolve, reject) => {
                            if(err.response.status == 401){
                                this.showAlert('Your token has expired. Please login',true);
                                setTimeout(()=>{
                                    this.$router.push('/');
                                    this.$EventBus.$emit('logout');
                                },2500);

                            }else if(err.response.status == 403){
                                app.$router.push('/error/403');
                            }else{
                                this.showAlert('Something went wrong! Please try again',true);

                            }
                            reject(err);
                        })
                    }).finally(()=>{
                        this.stop_loading();
                    })
            },
            get(url) {
                return this.ajaxCall('GET', url)
            },
            post(url,params ){
                return this.ajaxCall('POST', url, params )
            },
            patch(url,params ){
                return this.ajaxCall('PATCH', url, params )
            },
            delete(url) {
                return this.ajaxCall('DELETE', url)
            },
            redirect(url){
                window.location.replace(url);
            },
            emitPopup(text){
              let popup = {
                  snackbar:true,
                  text:text
              }
              this.$EventBus.$emit('popup',popup);
            },

            push_notification(img_url = '', body) {
                Notification.requestPermission( permission => {
                    let notification = new Notification(body, {
                        body: '', // content for the alert
                        icon: "https://pusher.com/static_logos/320x320.png" // optional image url
                    });

                    // link to page on clicking the notification
                    notification.onclick = () => {
                        window.open(window.location.href);
                    };
                });
            },
            convertDate(date, format = "DD-MM-YYYY") {
                if(date!=""){
                    let d = moment(date).format(format);
                    return d;
                }
                return "";
            },
            moment(){
                return moment();
            },
            showAlert(text,error){
                let alert = {
                    error:error,
                    show:true,
                    text:text
                }
                this.$EventBus.$emit('alert',alert);
            },
            start_loading(){
                this.$EventBus.$emit('loading');
            },
            stop_loading(){
                this.$EventBus.$emit('stop-loading');
            },
            autoLogin(redirect=false){
                let app = this;
                this.post(`auth/user`,{}).then(r=>{
                    app.$EventBus.$emit('auth');
                    if(redirect){
                        app.$router.push(`/home`)
                    }
                })
            }

        }
}

export default  mix;
