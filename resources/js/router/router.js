import Vue from 'vue'
import Router from 'vue-router'
import Home from "../components/Home"
import Login from "../components/Auth/Login"
import Register from "../components/Auth/Register"
import Diary from "../components/Diary/Diary"
import Workouts from "../components/Training/Workouts"
import Workout from "../components/Training/Workout";
import Progress from "../components/Progress/Progress";
import Friends from "../components/Friends/Friends";
import Friend from "../components/Friends/Friend";
import Blog from "../components/Blog/Blog";
import Post from "../components/Blog/Post";
import Settings from "../components/Settings/Settings";
import General from "../components/Settings/General";
import Account from "../components/Settings/Account/Account";
import Goals from "../components/Settings/Goals/Goals";
import Notifications from "../components/Settings/Notifications";
import Privacy from "../components/Settings/Privacy";
import About from "../components/Settings/About";
import store from "../store/store";
import Search from "../components/Food/Search";
import Food from "../components/Food/Food";
import Users from "../components/User/Users";
import User from "../components/User/User";

Vue.use(Router)

let router = new Router({
    routes: [
        {
            path: '/',
            name: 'Login',
            component:Login,

        },
        {
            path: '/register',
            name: 'Register',
            component:Register,

        },
        {
            path: '/home',
            name: 'Home',
            component:Home,
            meta: { requiresAuth: true }

        },
        {
            path: '/users',
            name: 'Users',
            component:Users,
            meta: { requiresAuth: true }

        },
        {
            path: '/users/user/:user_id',
            name: 'User',
            component:User,
            props: true,
            meta: { requiresAuth: true }

        },
        {
            path: '/diary',
            name: 'Diary',
            component:Diary,
            meta: { requiresAuth: true }

        },
        {
            path: '/diary/meal/:meal_id',
            name: 'Food',
            component:Food,
            props: true,
            meta: { requiresAuth: true }

        },
        {
            path: '/training',
            name: 'Workouts',
            component:Workouts,
            meta: { requiresAuth: true }

        },
        {
            path: '/training/workout/:workout_id',
            name: 'Workout',
            props:true,
            component:Workout,
            meta: { requiresAuth: true }

        },
        {
            path: '/progress',
            name: 'Progress',

            component:Progress,
            meta: { requiresAuth: true }

        },
        {
            path: '/friends',
            name: 'Friends',

            component:Friends,
            meta: { requiresAuth: true }

        },
        {
            path: '/friends/friend/:user_id',
            name: 'Friend',
            props:true,
            component:Friend,
            meta: { requiresAuth: true }

        },
        {
            path: '/blog',
            name: 'Blog',
            component:Blog,
            meta: { requiresAuth: true }

        },
        {
            path: '/blog/post/:post_id',
            name: 'Post',
            component:Post,
            props: true,
            meta: { requiresAuth: true }

        },
        {
            path: '/food/search',
            name: 'Search',
            component:Search,
            meta: { requiresAuth: true }

        },
        {
            path: '/food/add',
            name: 'Food',
            component:Food,
            meta: { requiresAuth: true }

        },
        {
            path: '/food/:id',
            name: 'Food',
            component:Food,
            meta: { requiresAuth: true },
            props:true

        },

        {
            path: '/settings/',
            name: 'Settings',
            meta: { requiresAuth: true },
            component:Settings,
            children:[
                {
                    path:'/',
                    components:{
                        b:General
                    }
                },
                {
                    path:'account/',
                    components:{
                        b:Account
                    }
                },
                {
                    path:'goals/',
                    components:{
                        b:Goals
                    }
                },
                {
                    path:'notifications/',
                    components:{
                        b:Notifications
                    }
                },
                {
                    path:'privacy/',
                    components:{
                        b:Privacy
                    }
                },
                {
                    path:'about/',
                    components:{
                        b:About
                    }
                },
            ]

        },
    ]
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        let now = new Date();
        if (now > localStorage.getItem('expires_in')) {
            next({
                path: '/',
            })
        } else {
            store.commit('setAuth');

            next()

        }
    } else {
        next() // make sure to always call next()!
    }
})
export  default router
