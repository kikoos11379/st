require('./bootstrap');

import App from './components/App'
import Vue from 'vue'
import store from './store/store'
import router from "./router/router"
import vuetify from "./vuetify/vuetify"
import axios from 'axios';
import mix from './mixins/mixin';

axios.defaults.baseURL = "http://localhost:8000/api/"
Vue.prototype.$EventBus = new Vue();




Vue.mixin(mix);


const app = new Vue({
    el: '#app',
    template: '<App/>',
    vuetify,
    store,
    router,

    components: { App }
});

